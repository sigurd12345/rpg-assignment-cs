﻿namespace RPG
{
    public class Weapon : Item
    {
        public double WeaponDamage;
        public double AttacksPerSecond;
        public WeaponType WeaponType;

        public override EquipmentSlot EquipmentSlot => EquipmentSlot.Weapon;
        public double DPS => WeaponDamage * AttacksPerSecond;

        public override string SuccessfulEquipMessage => "New weapon equipped!";

        public Weapon(string name, int levelRequirement, double weaponDamage, double attacksPerSecond, WeaponType weaponType, Attributes AttributeBonus)
            : base(name, levelRequirement, AttributeBonus)
        {
            this.WeaponDamage = weaponDamage;
            this.AttacksPerSecond = attacksPerSecond;
            this.WeaponType = weaponType;
        }


        public override void CheckEquipment(Character character)
        {
            if (LevelRequirement > character.Level)
            {
                throw new InvalidWeaponException("Item level is higher than character level");
            }
            if (!character.WeaponSuitsCharacter(this))
            {
                throw new InvalidWeaponException("Character type " + this.GetType() + " cannot equip weapon of type " + WeaponType.ToString());
            }
        }
    }
}
﻿
using System.Text;

namespace RPG
{
    public static class CharacterSheetMaker
    {
        /// <summary>
        /// Creates a charactersheet for a given character.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public static string CreateSheet(Character character)
        {
            StringBuilder builder = new StringBuilder();
            Attributes attr = character.TotalAttributes();
            double DPS = character.CalculateTotalDamage();
            var splitTypeWithNamespace = character.GetType().ToString().Split('.');
            string Job = splitTypeWithNamespace[splitTypeWithNamespace.Length - 1];

            builder.Append("Name: " + character.Name + '\n');
            builder.Append("Level: " + character.Level + '\n');
            builder.Append("Job: " + Job + '\n');
            builder.Append("Strength: " + attr.Strength + '\n');
            builder.Append("Dexterity: " + attr.Dexterity + '\n');
            builder.Append("Intelligence: " + attr.Intelligence + '\n');
            builder.Append("DPS: " + DPS + '\n');

            string S = builder.ToString();
            return S;
        }
    }
}


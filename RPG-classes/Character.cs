﻿

namespace RPG
{
    public abstract class Character
    {
        public Attributes CharacterAttributes { get; protected set; } = new();
        public abstract Attributes AttributeGrowthPerLevel { get; }
        Dictionary<EquipmentSlot, Item> Items = new();

        /// <summary>
        /// Gets the int value of the character's main attribute, which is used to calculate damage.
        /// </summary>
        public abstract int GetMainAttribute(Attributes p);
        public string Name { get; private set; }
        public int Level { get; private set; }


        /// <summary>
        /// Base constructor. Throws an exception if level is less than 0
        /// </summary>
        public Character(string name)
        {
            Name = name;
            Level = 1;
        }

        public void GainLevel()
        {
            Level++;
            CharacterAttributes += AttributeGrowthPerLevel;
        }

        /// <summary>
        /// Calculates a character's total damage output (DPS). Returns type double.
        /// </summary>
        public double CalculateTotalDamage()
        {
            Weapon? weapon = Items.ContainsKey(EquipmentSlot.Weapon) ? Items[EquipmentSlot.Weapon] as Weapon : null;
            double weaponDPS = 1;
            if (weapon != null)
                weaponDPS = weapon.DPS;
            return weaponDPS * (1 + GetMainAttribute(TotalAttributes()) / 100);
        }
        /// <summary>
        /// Calculates and returns the total attribute the character has, from base attributes + levels + equipment.
        /// </summary>
        public Attributes TotalAttributes()
        {
            Attributes output = CharacterAttributes;
            foreach (KeyValuePair<EquipmentSlot, Item> item in Items)
            {
                output += item.Value.PrimaryAttribute;
            }
            return output;
        }
        /// <summary>
        /// Makes the character attempt to equip a given piece of equipment.
        /// Returns a succsess-message (string) if successful. Otherwise, it throws an error.
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="NullReferenceException"></exception>
        /// <exception cref="InvalidArmorException"></exception>
        /// <exception cref="InvalidWeaponException"></exception>
        public string TryEquip(Item item)
        {
            item.CheckEquipment(this);
            Items.Add(item.EquipmentSlot, item);
            return item.SuccessfulEquipMessage;
        }
        /// <summary>
        /// Checks whether a given armor piece can be equipped by the character.
        /// Returns true/false.
        /// </summary>
        public abstract bool ArmorSuitsCharacter(Armor item);
        /// <summary>
        /// Checks whether a given weapon can be equipped by the character.
        /// Returns true/false.
        /// </summary>
        public abstract bool WeaponSuitsCharacter(Weapon item);
    }
}


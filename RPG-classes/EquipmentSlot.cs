﻿namespace RPG
{
    public enum EquipmentSlot
    {
        Head,
        Body,
        Legs,
        Weapon
    }

}
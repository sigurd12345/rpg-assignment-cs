﻿namespace RPG
{
    public class Armor : Item
    {
        public ArmorType ArmorType;
        public override EquipmentSlot EquipmentSlot { get; }

        public override string SuccessfulEquipMessage => "New armour equipped!";

        public Armor(string Name, int LevelRequirement, ArmorType armorType, Attributes AttributeBonus, EquipmentSlot EquipmentSlot) 
            : base(Name, LevelRequirement, AttributeBonus)
        {
            ArmorType = armorType;
            this.EquipmentSlot = EquipmentSlot;
        }

        public override void CheckEquipment(Character character)
        {
            if (LevelRequirement > character.Level)
            {
                throw new InvalidArmorException("Item level is higher than character level");
            }
            if (!character.ArmorSuitsCharacter(this))
            {
                throw new InvalidArmorException("Character type " + this.GetType() + " cannot equip armor of type " + ArmorType.ToString());
            }
        }
    }
}
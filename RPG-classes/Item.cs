﻿namespace RPG
{
    public abstract class Item
    {
        public string Name;
        public int LevelRequirement;
        public abstract EquipmentSlot EquipmentSlot { get; }
        public Attributes PrimaryAttribute;
        public abstract string SuccessfulEquipMessage { get; }
        /// <summary>
        /// Check whether this equipment can be equipped to this character.
        /// </summary>
        /// <param name="character"></param>
        /// <exception cref="InvalidArmorException"></exception>
        /// <exception cref="InvalidWeaponException"></exception>
        public abstract void CheckEquipment(Character character);
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="levelRequirement"></param>
        /// <param name="primaryAttribute"></param>
        public Item(string name, int levelRequirement, Attributes primaryAttribute)
        {
            Name = name;
            LevelRequirement = levelRequirement;
            PrimaryAttribute = primaryAttribute;
        }
    }
}
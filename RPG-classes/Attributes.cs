﻿namespace RPG
{
    public struct Attributes
    {
        public int Strength;
        public int Dexterity;
        public int Intelligence;

        public Attributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        public static Attributes operator +(Attributes a, Attributes b)
        {
            return new Attributes(a.Strength + b.Strength, a.Dexterity + b.Dexterity, a.Intelligence + b.Intelligence);
        }
        public static Attributes operator *(Attributes a, int b)
        {
            return new Attributes(a.Strength * b, a.Dexterity * b, a.Intelligence * b);
        }
    }
}

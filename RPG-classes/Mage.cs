﻿namespace RPG
{
    public class Mage : Character
    {
        public Mage(string name) : base(name)
        {
            CharacterAttributes = new Attributes(1, 1, 8);
        }
        public override Attributes AttributeGrowthPerLevel => new Attributes(1, 1, 5);
        public override int GetMainAttribute(Attributes p)
        {
            return p.Intelligence;
        }


        public override bool WeaponSuitsCharacter(Weapon item)
        {
            return (item.WeaponType == WeaponType.Wand || item.WeaponType == WeaponType.Staff);
        }
        public override bool ArmorSuitsCharacter(Armor item)
        {
            return (item.ArmorType == ArmorType.Cloth);
        }
    }
}
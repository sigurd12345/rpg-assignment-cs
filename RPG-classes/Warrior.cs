﻿namespace RPG
{
    public class Warrior : Character
    {
        public Warrior(string name) : base(name)
        {
            CharacterAttributes = new Attributes(5, 2, 1);
        }
        public override Attributes AttributeGrowthPerLevel => new Attributes(3, 2, 1);
        public override int GetMainAttribute(Attributes p)
        {
            return p.Strength;
        }


        public override bool WeaponSuitsCharacter(Weapon item)
        {
            return (item.WeaponType == WeaponType.Hammer || item.WeaponType == WeaponType.Axe || item.WeaponType == WeaponType.Sword);
        }
        public override bool ArmorSuitsCharacter(Armor item)
        {
            return (item.ArmorType == ArmorType.Plate || item.ArmorType == ArmorType.Mail);
        }
    }

}
﻿namespace RPG
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
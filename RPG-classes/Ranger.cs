﻿namespace RPG
{
    public class Ranger : Character
    {
        public Ranger(string name) : base(name)
        {
            CharacterAttributes = new Attributes(1, 7, 1);
        }
        public override Attributes AttributeGrowthPerLevel => new Attributes(1, 5, 1);
        public override int GetMainAttribute(Attributes p)
        {
            return p.Dexterity;
        }

        public override bool WeaponSuitsCharacter(Weapon item)
        {
            return (item.WeaponType == WeaponType.Bow);
        }
        public override bool ArmorSuitsCharacter(Armor item)
        {
            return (item.ArmorType == ArmorType.Leather || item.ArmorType == ArmorType.Mail);
        }
    }

}
﻿namespace RPG
{
    public class Rogue : Character
    {
        public Rogue(string name) : base(name)
        {
            CharacterAttributes = new Attributes(2, 6, 1);
        }
        public override Attributes AttributeGrowthPerLevel => new Attributes(1, 4, 1);

        public override int GetMainAttribute(Attributes p)
        {
            return p.Dexterity;
        }

        public override bool WeaponSuitsCharacter(Weapon item)
        {
            return (item.WeaponType == WeaponType.Dagger || item.WeaponType == WeaponType.Sword);
        }
        public override bool ArmorSuitsCharacter(Armor item)
        {
            return (item.ArmorType == ArmorType.Leather || item.ArmorType == ArmorType.Mail);
        }
    }

}
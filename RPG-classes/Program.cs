﻿namespace RPG
{
    public static class Program
    {
        static void Main()
        {
            Character character = new Mage("Jorbs");
            while(character.Level < 12)
            {
                character.GainLevel();
            }
            Weapon weapon = new("Great Wand", 6, 12.1, 2.7, WeaponType.Wand, new Attributes(5, 10, 28));
            character.TryEquip(weapon);
            string sheet = CharacterSheetMaker.CreateSheet(character);
            Console.WriteLine(sheet);
        }
    }
}
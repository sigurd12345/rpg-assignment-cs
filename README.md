## RPG Classes

This project consists of a suite of classes which form the foundation of an RPG game.


## Table of Contents

- [Install](#Install)
- [Run](#Run)
- [Author](#Author)

## Install

Install Visual Studio.
Clone the repository and open it in Visual Studio.
This project was made using Visual Studio 2022 and .NET6.0 - it has not been tested on older versions.

## Run

Run the program in Visual Studio in the tab Debug->Start without debugging.
Running the program will create a mage character, assign him a weapon and armor, and print his character sheet.
You can run all unit tests in Visual Studio by under the tab Test->Run all tests.

## Author

@srh12345 / Sigurd Riis Haugen

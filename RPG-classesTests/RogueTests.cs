﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPG;

namespace RPG_Tests
{
    [TestClass()]
    public class RogueTests
    {
        [TestMethod]
        public void Constructor_TestAttributes_ShouldBeCorrect()
        {
            Character character = new Rogue("bob");

            Attributes attr = character.CharacterAttributes;

            Assert.AreEqual(attr.Strength, 2);
            Assert.AreEqual(attr.Dexterity, 6);
            Assert.AreEqual(attr.Intelligence, 1);
        }
        [TestMethod]
        public void GainLevel_TestRogueAttributes_ShouldBeCorrect()
        {
            Character character = new Rogue("bob");

            character.GainLevel();
            Attributes attr = character.CharacterAttributes;

            Assert.AreEqual(attr.Strength, 3);
            Assert.AreEqual(attr.Dexterity, 10);
            Assert.AreEqual(attr.Intelligence, 2);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPG;

namespace RPG_Tests
{
    [TestClass()]
    public class RangerTests
    {
        [TestMethod]
        public void Constructor_TestAttributes_ShouldBeCorrect()
        {
            Character character = new Ranger("bob");

            Attributes attr = character.CharacterAttributes;

            Assert.AreEqual(attr.Strength, 1);
            Assert.AreEqual(attr.Dexterity, 7);
            Assert.AreEqual(attr.Intelligence, 1);
        }
        [TestMethod]
        public void GainLevel_TestRangerAttributes_ShouldBeCorrect()
        {
            Character character = new Ranger("bob");

            character.GainLevel();
            Attributes attr = character.CharacterAttributes;

            Assert.AreEqual(attr.Strength, 2);
            Assert.AreEqual(attr.Dexterity, 12);
            Assert.AreEqual(attr.Intelligence, 2);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPG;
using System;

namespace RPG_Tests
{
    [TestClass()]
    public class MageTests
    {
        [TestMethod]
        public void MageConstructor_TestAttributes_ShouldBeCorrect()
        {
            Character mage = new Mage("bob");

            Attributes attr = mage.CharacterAttributes;

            Assert.AreEqual(attr.Strength, 1);
            Assert.AreEqual(attr.Dexterity, 1);
            Assert.AreEqual(attr.Intelligence, 8);
        }
        
        
        
        [TestMethod]
        public void GainLevel_TestMageAttributes_ShouldBeCorrect()
        {
            Character character = new Mage("bob");

            character.GainLevel();
            Attributes attr = character.CharacterAttributes;

            Assert.AreEqual(attr.Strength, 2);
            Assert.AreEqual(attr.Dexterity, 2);
            Assert.AreEqual(attr.Intelligence, 13);
        }
        
        
        

    }
}

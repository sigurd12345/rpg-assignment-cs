﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPG;
using System;

namespace RPG.Tests
{
    [TestClass()]
    public class CharacterTests
    {
        [TestMethod]
        public void Constructor_CreateCharacter_CharacterIsLevel1()
        {
            Character mage = new Mage("bob");
            Character warrior = new Warrior("bob");
            Character rogue = new Rogue("bob");
            Character ranger = new Ranger("bob");

            bool allEqual1 = (mage.Level == 1) 
                && (warrior.Level == 1) 
                && (rogue.Level == 1) 
                && (ranger.Level == 1);

            Assert.IsTrue(allEqual1);
        }

        [TestMethod]
        public void GainLevel_Test_ShouldAddOne()
        {
            Character mage = new Mage("bob");

            mage.GainLevel();

            Assert.AreEqual(mage.Level, 2);
        }

        [TestMethod]
        public void TryEquip_TestHighLevelWeapon_ShouldThrowInvalidWeapon()
        {
            //If a character tries to equip a high level weapon, InvalidWeaponException should be thrown.
            Character character = new Warrior("Steve");
            Item highLevelWeapon = new Weapon("cleaver", 2, 20, 1, WeaponType.Axe, new Attributes(10, 10, 10));
            
            Assert.ThrowsException<InvalidWeaponException>(() => character.TryEquip(highLevelWeapon));

        }

        [TestMethod()]
        public void TryEquip_TestHighLevelArmor_ShouldThrowInvalidArmor()
        {
            //If a character tries to equip a high level armor piece, InvalidArmorException should be thrown.
            Character character = new Warrior("Steve");
            Item highLevelArmor = new Armor("big plate", 10, ArmorType.Plate, new Attributes(1, 1, 1), EquipmentSlot.Body);

            Action action = () => character.TryEquip(highLevelArmor);

            Assert.ThrowsException<InvalidArmorException>(action);
            
        }

        [TestMethod()]
        public void TryEquip_TestWrongWeapon_ShouldThrowInvalidWeapon()
        {
            //If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown
            Character character = new Warrior("Steve");
            Item bow = new Weapon("long bow", 0, 1, 1, WeaponType.Bow, new Attributes(1, 1, 1));

            Action action = () => character.TryEquip(bow);

            Assert.ThrowsException<InvalidWeaponException>(action);
            
        }

        [TestMethod()]
        public void TryEquip_TestWrongArmor_ShouldThrowInvalidArmor()
        {
            //If a character tries to equip the wrong armor type, InvalidArmorException should be thrown.
            Character character = new Warrior("Steve");
            Item wrongArmor = new Armor("dumb chainmail", 0, ArmorType.Cloth, new Attributes(0, 0, 0), EquipmentSlot.Body);

            Action action = () => character.TryEquip(wrongArmor);

            Assert.ThrowsException<InvalidArmorException>(action);

        }

        [TestMethod()]
        public void TryEquip_TestCorrectWeapon_ShouldReturMessage()
        {
            //If a character equips a valid weapon, a success message should be returned
            Character character = new Warrior("Steve");
            Weapon validWeapon = new Weapon("basic axe", 0, 7, 1.1, WeaponType.Axe, new Attributes(0, 0, 0));
            
            string message = character.TryEquip(validWeapon);

            Assert.IsTrue(message == "New weapon equipped!");
        }

        [TestMethod()]
        public void TryEquip_TestValidArmor_ShouldReturnMessage()
        {
            //If a character equips a valid armor piece, a success message should be returned
            Character character = new Warrior("Steve");
            Armor validArmor = new Armor("basic plate", 0, ArmorType.Plate, new Attributes(1, 1, 1), EquipmentSlot.Body);
            
            string message = character.TryEquip(validArmor);
            
            Assert.IsTrue(message == "New armour equipped!");
        }

        [TestMethod()]
        public void CalculateTotalDamage_TestWithNoItems_ShouldReturnCorrectNumber()
        {
            //Calculate Damage if no weapon is equipped.
            Character newCharacter = new Warrior("Steve");
            
            double damage = newCharacter.CalculateTotalDamage();
            double expectedDamage = 1 + (5.0 / 100.0);
            bool areEqual = Math.Abs(damage - expectedDamage) > double.Epsilon;

            Assert.IsTrue(areEqual);
        }

        [TestMethod()]
        public void CalculateTotalDamage_TestWithWeapon_ShouldReturnCorrectNumber()
        {
            // Calculate Damage with valid weapon equipped.
            Character newCharacter = new Warrior("Steve");
            Weapon validWeapon = new Weapon("basic axe", 0, 7, 1.1, WeaponType.Axe, new Attributes(0, 0, 0));
            newCharacter.TryEquip(validWeapon);
            
            double damage = newCharacter.CalculateTotalDamage();
            double  expectedDamage = (7 * 1.1) * (1 + (5.0 / 100));
            bool condition = Math.Abs(damage - expectedDamage) > double.Epsilon;

            Assert.IsTrue(condition);
        }

        [TestMethod()]
        public void CalculateTotalDamage_TestWithWeaponAndArmor_ShouldReturnCorrectNumber()
        {
            //Calculate Damage with valid weapon and armor equipped
            Character newCharacter = new Warrior("Steve");
            Armor validArmor = new Armor("basic plate", 0, ArmorType.Plate, new Attributes(1, 1, 1), EquipmentSlot.Body);
            Weapon validWeapon = new Weapon("basic axe", 0, 7, 1.1, WeaponType.Axe, new Attributes(0, 0, 0));
            newCharacter.TryEquip(validArmor);
            newCharacter.TryEquip(validWeapon);
            
            double damage = newCharacter.CalculateTotalDamage();
            double expectedDamage = (7 * 1.1) * (1 + ((5.0 + 1.0) / 100));
            bool condition = Math.Abs(damage - expectedDamage) > double.Epsilon;

            Assert.IsTrue(condition);
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RPG;

namespace RPG_Tests
{
    [TestClass()]
    public class WarriorTests
    {
        [TestMethod]
        public void WarriorConstructor_TestAttributes_ShouldBeCorrect()
        {
            Character character = new Warrior("bob");

            Attributes attr = character.CharacterAttributes;

            Assert.AreEqual(attr.Strength, 5);
            Assert.AreEqual(attr.Dexterity, 2);
            Assert.AreEqual(attr.Intelligence, 1);
        }
        [TestMethod]
        public void GainLevel_TestWarriorAttributes_ShouldBeCorrect()
        {
            Character character = new Warrior("bob");

            character.GainLevel();
            Attributes attr = character.CharacterAttributes;

            Assert.AreEqual(attr.Strength, 8);
            Assert.AreEqual(attr.Dexterity, 4);
            Assert.AreEqual(attr.Intelligence, 2);
        }
    }
}
